"""
libmineshaft
~~~~~~~~~~
This module contains the metadata: The version, author name, and the credits.
"""
__version__ = "0.1.6rc-1"
__author__ = "Double Fractal Game Studios"
__credits__ = """Double Fractal Game Studios team:
*   Mayu Sakurai
*   Alexey Pavlov
"""
